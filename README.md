#Javashop-B2C
Javashop是基于Java技术构建的开源电子商务平台，采用EOP(Enation Open Platform易族开放平台)框架体系，更具人性化的操作体验，内置库存管理系统，完备的订单流程，丰富的统计功能，多种支付方式，适合搭建稳定、高效的B2C电子商务平台。 同时Javashop的模板引擎及组件机制让扩展变得简单，更有大量第三方组件可供选择，极大的降低二次开发成本。

#关于数据库

 **1. 我们的数据库是存放在XML文件中的，程序初始化时会判断是否已安装;** 

 **2. 是否已安装的判断条件是:webapp/install目录下中是否有“install.lock”文件;** 

 **3. 如果有：则读取webapp/config目录下jdbc数据库配置文件连接数据库;** 

 **4. 如果没有：则跳过，首次访问时会进入安装向导，填写数据库的连接配置，包括：数据库连接地址、用户名、密码、待安装的数据库名称等，填写完毕后，安装向导会读取XML中数据库信息，进行数据库初始化操作，安装完毕之后，会在webapp/install目录下自动创建“install.lock”文件标记已安装成功!** 



#开源计划

暂无其他计划

# 关于商用

源码免费学习，但是商用需要授权，联系QQ：3413414

#开发环境搭建

搭建前准备：jdk8、maven、tomcat、idea、MySQL

1. 首先先把b2c项目webapp中config目录中的两个文件example后缀去掉，改为正常的properties配置文件

   ![image-20200629181111295](assets/image-20200629181111295.png)

2. 使用idea打开 javashop目录，源码都在该目录下

   ![image-20200629180119333](assets/image-20200629180119333.png)

   如果没有自动识别为maven项目，请手动增加为maven项目

3. 添加tomcat

   ![image-20200629180607873](assets/image-20200629180607873.png)

   ![image-20200629180631777](assets/image-20200629180631777.png)

   ![image-20200629180705634](assets/image-20200629180705634.png)

   ![image-20200629180803118](assets/image-20200629180803118.png)

   ![image-20200629180900851](assets/image-20200629180900851.png)

4. 启动tomcat即可看到该页面，按照提示进行数据库安装即可

   ![image-20200629181200789](assets/image-20200629181200789.png)

官网网站：

http://www.shoptnt.cn

http://www.javashop.cn

http://www.javamall.com.cn




















